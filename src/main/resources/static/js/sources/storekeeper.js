function downloadItems(response) {
    $.each(response, function(i, item) {
        let row =
            "<tr>" +
            "<td><input type='checkbox' name='record'></td>" +
            "<td>" + item.name + "</td>" +
            "<td>" + item.price + "</td>" +
            "<td>" + item.category.name + "</td>" +
            "<td>" + item.description + "</td>" +
            "</tr>";
        $("table tbody").append(row);
    });
}

function addItem(name, price, categoryName, description) {
    if (categoryName === "Phones") {
        id = 1;
    } else if (categoryName === "Laptops") {
        id = 2;
    } else {
        id = 3;
    }

    $.ajax({
        url: 'https://localhost:8080/item/save',
        type: 'POST',
        data: JSON.stringify({ "name": name, "price" : price, "category": {"id": id, "name": name}, "description": description }),
        contentType: 'application/json',
    });
}

function deleteItemByName(name) {
    $.ajax({
        url: 'https://localhost:8080/item/delete/' + name,
        type: 'DELETE',
    });
}

$(document).ready(function(){
    $("#locales").change(function () {
        var selectedOption = $('#locales').val();
        if (selectedOption !== ''){
            window.location.replace('/?lang=' + selectedOption);
        }
    });

    $.ajax({
        url: 'https://localhost:8080/item/all',
        type: 'GET',
        dataType: 'json',
        success: function(result) {
            console.log(result);
            downloadItems(result);
        },
        error: function () {
            alert("failed to connect to database");
        }
    });

    $(".add-item").click(function(){
        let name = $("#name").val();
        let price = $("#price").val();
        let categoryName = $("#categoryName option:selected").text();
        let description = $("#description").val();
        let row =
            "<tr>" +
            "<td>" +
            "<input type='checkbox' name='record'>" +
            "</td>" +
            "<td>" + name + "</td>" +
            "<td>" + price + "</td>" +
            "<td>" + categoryName + "</td>" +
            "<td>" + description + "</td>" +
            "</tr>";
        addItem(name, price, categoryName, description);
        $("table tbody").append(row);
    });

    $(".delete-item").click(function(){
        $("table tbody").find('input[name="record"]').each(function(){
            if($(this).is(":checked")){
                let name = $(this).closest("tr").find("td:eq(1)").text();
                deleteItemByName(name);
                $(this).parents("tr").remove();
            }
        });
    });
});
