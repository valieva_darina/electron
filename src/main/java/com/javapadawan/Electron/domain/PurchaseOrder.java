package com.javapadawan.Electron.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity(name = "purchase_order")
public class PurchaseOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToMany
    private Set<Item> items;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private StoreBranch storeBranch;
    @Column(name = "purchase_date")
    private LocalDate purchaseDate;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    public PurchaseOrder() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchaseOrder that = (PurchaseOrder) o;
        return Objects.equals(customer, that.customer) && Objects.equals(storeBranch, that.storeBranch) && Objects.equals(purchaseDate, that.purchaseDate) && Objects.equals(expirationDate, that.expirationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, storeBranch, purchaseDate, expirationDate);
    }

    public String toErrorString() {
        return id + "-" + customer + "-" + storeBranch.getId() + "-" + purchaseDate.toString() + "-" + expirationDate.toString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public StoreBranch getStoreBranch() {
        return storeBranch;
    }

    public void setStoreBranch(StoreBranch storeBranch) {
        this.storeBranch = storeBranch;
    }

    public LocalDate getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(LocalDate purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
        this.expirationDate = expirationDate;
    }
}
