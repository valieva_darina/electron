package com.javapadawan.Electron.service;

import com.javapadawan.Electron.domain.Customer;
import com.javapadawan.Electron.domain.Item;
import com.javapadawan.Electron.domain.PurchaseOrder;
import com.javapadawan.Electron.repository.CustomerRepository;
import com.javapadawan.Electron.repository.ItemRepository;
import com.javapadawan.Electron.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ShopService implements StoreKeeper, PurchaseCoordinator, CustomerManager {
    private final ItemRepository itemRepo;
    private final PurchaseRepository purchaseRepo;
    private final CustomerRepository customerRepo;

    @Autowired
    public ShopService(CustomerRepository customerRepo, PurchaseRepository purchaseRepo, ItemRepository itemRepo) {
        this.customerRepo = customerRepo;
        this.itemRepo = itemRepo;
        this.purchaseRepo = purchaseRepo;
    }

    //item stuff
    @Override
    public void addItem(Item item) {
        List<Item> items = itemRepo.findAll();
        if (items.contains(item)) {
            throw new IllegalArgumentException("The item '" + item.toErrorString() + "' already exists");
        }
        itemRepo.save(item);
    }

    @Override
    public void deleteItemById(long id) {
        Optional<Item> itemToDelete = itemRepo.findById(id);
        if (itemToDelete.isEmpty()) {
            throw new IllegalArgumentException("The item with id '" + id + "' does not exists");
        }
        if (itemIsInUse(itemToDelete.get())) {
            throw new IllegalArgumentException("The item with id '" + id + "' is being used in purchases");
        }
        itemRepo.deleteById(id);
    }

    @Override
    public void updateItemOnId(Item sample) {
        Optional<Item> itemToUpdate = itemRepo.findById(sample.getId());
        if (itemToUpdate.isEmpty()) {
            throw new IllegalArgumentException("The item with id '" + sample.getId() + "' does not exists");
        }
        if (itemIsInUse(itemToUpdate.get())) {
            throw new IllegalArgumentException("The item with id '" + sample.getId() + "' is being used in purchases");
        }
        if (sample.getName() != null) {
            itemToUpdate.get().setName(sample.getName());
        }
        if (sample.getCategory() != null) {
            itemToUpdate.get().setCategory(sample.getCategory());
        }
        if (sample.getDescription() != null) {
            itemToUpdate.get().setDescription(sample.getDescription());
        }
        if (sample.getPrice() != 0) {
            itemToUpdate.get().setPrice(sample.getPrice());
        }
        itemRepo.save(itemToUpdate.get());
    }

    @Override
    public List<Item> getAllItems() {
        return itemRepo.findAll();
    }

    @Override
    public Item getItemById(long id) {
        Optional<Item> itemToFind = itemRepo.findById(id);
        if (itemToFind.isEmpty()) {
            throw new IllegalArgumentException("The item with id '" + id + "' does not exist");
        }
        return itemToFind.get();
    }

    @Override
    public List<Item> getItemsByCategoryName(String categoryName) {
        return itemRepo.findItemsByCategoryName(categoryName);
    }

    @Override
    public List<Item> getItemsBySample(Item sample) {
        return itemRepo.findAll(Example.of(sample));
    }

    @Override
    @Transactional
    public void deleteItemByName(String name) {
        itemRepo.deleteByName(name);
    }

    //purchase stuff
    @Override
    public void addPurchase(PurchaseOrder purchase) {
        List<PurchaseOrder> purchases = purchaseRepo.findAll();
        if (purchases.contains(purchase)) {
            throw new IllegalArgumentException("The purchase order '" + purchase.toErrorString() + "' already exists");
        }
        purchaseRepo.save(purchase);
    }

    @Override
    public void deletePurchaseById(long id) {
        Optional<PurchaseOrder> purchaseToDelete = purchaseRepo.findById(id);
        if (purchaseToDelete.isEmpty()) {
            throw new IllegalArgumentException("The purchase order with id'" + id + "' does not exist");
        }
        purchaseRepo.deleteById(id);
    }

    @Override
    public void updatePurchaseOnId(PurchaseOrder sample) {
        Optional<PurchaseOrder> purchaseToUpdate = purchaseRepo.findById(sample.getId());
        if (purchaseToUpdate.isEmpty()) {
            throw new IllegalArgumentException("The purchase order with id'" + sample.getId() + "' does not exist");
        }
        if (sample.getCustomer() != null) {
            purchaseToUpdate.get().setCustomer(sample.getCustomer());
        }
        if (sample.getPurchaseDate() != null) {
            purchaseToUpdate.get().setPurchaseDate(sample.getPurchaseDate());
        }
        if (sample.getExpirationDate() != null) {
            purchaseToUpdate.get().setExpirationDate(sample.getExpirationDate());
        }
        if (sample.getStoreBranch() != null) {
            purchaseToUpdate.get().setStoreBranch(sample.getStoreBranch());
        }
        purchaseRepo.save(purchaseToUpdate.get());
    }

    @Override
    public PurchaseOrder getPurchaseById(long id) {
        Optional<PurchaseOrder> purchaseToFind = purchaseRepo.findById(id);
        if (purchaseToFind.isEmpty()) {
            throw new IllegalArgumentException("The purchase order with id '" + id + "' does not exist");
        }
        return purchaseToFind.get();
    }

    @Override
    public List<PurchaseOrder> getAllPurchases() {
        return purchaseRepo.findAll();
    }

    @Override
    public List<PurchaseOrder> getPurchasesByCustomerId(long customerId) {
        return purchaseRepo.findAllByCustomerId(customerId);
    }

    @Override
    public List<PurchaseOrder> getPurchasesByStoreId(long id) {
        return purchaseRepo.findAllByStoreBranchId(id);
    }

    @Override
    public List<PurchaseOrder> getPurchasesByDates(LocalDate dateFrom, LocalDate dateTo) {
        return purchaseRepo.findAllByPurchaseDateBetween(dateFrom, dateTo);
    }

    @Override
    public List<PurchaseOrder> getPurchasesByDates(LocalDate purchaseDate) {
        return purchaseRepo.findAllByPurchaseDate(purchaseDate);
    }

    //customer stuff
    @Override
    public void addCustomer(Customer customer) {
        List<Customer> customers = customerRepo.findAll();
        if (customers.contains(customer)) {
            throw new IllegalArgumentException("The customer with email '" + customer.getEmail() + "' already exists");
        }
        customerRepo.save(customer);
    }

    @Override
    public void deleteCustomerById(long id) {
        Optional<Customer> customerToDelete = customerRepo.findById(id);
        if (customerToDelete.isEmpty()) {
            throw new IllegalArgumentException("The customer with id '" + id + "' does not exist");
        }
        customerRepo.deleteById(id);
    }

    @Override
    public void updateCustomerOnId(Customer sample) {
        Optional<Customer> customerToUpdate = customerRepo.findById(sample.getId());
        if (customerToUpdate.isEmpty()) {
            throw new IllegalArgumentException("The customer with id '" + sample.getId() + "' does not exist");
        }
        if (sample.getEmail() != null) {
            Customer customersByEmail = customerRepo.findByEmail(sample.getEmail());
            if (customersByEmail != null) {
                throw new IllegalArgumentException("The email address '" + sample.getEmail() + "' already in use");
            }
            customerToUpdate.get().setEmail(sample.getEmail());
        }
        if (sample.getName() != null) {
            customerToUpdate.get().setName(sample.getName());
        }
        if (sample.getLastName() != null) {
            customerToUpdate.get().setLastName(sample.getLastName());
        }
        customerRepo.save(customerToUpdate.get());
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepo.findAll();
    }

    @Override
    public Customer getCustomerById(long id) {
        Optional<Customer> customerById = customerRepo.findById(id);
        if (customerById.isEmpty()) {
            throw new IllegalArgumentException("The customer with id '" + id + "' does not exist");
        }
        return customerById.get();
    }

    @Override
    public Customer getCustomerByEmail(String email) {
        return customerRepo.findByEmail(email);
    }

    //private methods
    private boolean itemIsInUse(Item item) {
        return !item.getOrders().isEmpty();
    }
}
