package com.javapadawan.Electron.controller;

import com.javapadawan.Electron.domain.PurchaseOrder;
import com.javapadawan.Electron.service.PurchaseCoordinator;
import com.javapadawan.Electron.service.ShopService;
import org.hibernate.boot.model.naming.IllegalIdentifierException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {
    private final PurchaseCoordinator coordinator;

    @Autowired
    public PurchaseController(ShopService coordinator) {
        this.coordinator = coordinator;
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<PurchaseOrder> getAll() {
        return coordinator.getAllPurchases();
    }

    @GetMapping("/{id}")
    public PurchaseOrder getById(@PathVariable long id) {
        try {
            return coordinator.getPurchaseById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/store/{storeId}")
    public List<PurchaseOrder> getAllByStoreId(@PathVariable long storeId) {
        return coordinator.getPurchasesByStoreId(storeId);
    }

    @GetMapping("/customer/{customerId}")
    public List<PurchaseOrder> getAllByCustomerId(@PathVariable long customerId) {
        return coordinator.getPurchasesByCustomerId(customerId);
    }

    @GetMapping("/dateRange")
    public List<PurchaseOrder> getAllByDates(@RequestParam("dateFrom") String dateFrom,
                                             @RequestParam("dateTo") String dateTo) {
        return coordinator.getPurchasesByDates(LocalDate.parse(dateFrom), LocalDate.parse(dateTo));
    }

    @GetMapping("/date")
    public List<PurchaseOrder> getAllByDate(@RequestParam("date") String date) {
        return coordinator.getPurchasesByDates(LocalDate.parse(date));
    }

    @PostMapping("/save")
    public void add(@RequestBody PurchaseOrder purchase) {
        try {
            coordinator.addPurchase(purchase);
        } catch(IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PutMapping("/save")
    public void update(@RequestBody PurchaseOrder purchaseToUpdate) {
        try {
            coordinator.updatePurchaseOnId(purchaseToUpdate);
        } catch(IllegalIdentifierException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable long id) {
        try {
            coordinator.deletePurchaseById(id);
        } catch (IllegalIdentifierException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
